from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

# Connect to Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///cafes.db'
app.config['SECRET_KEY'] = 'TopSecretAPIKey'
db = SQLAlchemy()
db.init_app(app)

class Cafe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250), unique=True, nullable=True)
    map_url = db.Column(db.String(500), nullable=True)
    img_url = db.Column(db.String(500), nullable=True)
    location = db.Column(db.String(250), nullable=True)
    seats = db.Column(db.String(250), nullable=True)
    has_toilet = db.Column(db.Boolean, nullable=True)
    has_wifi = db.Column(db.Boolean, nullable=True)
    has_sockets = db.Column(db.Boolean, nullable=True)
    can_take_calls = db.Column(db.Boolean, nullable=True)
    coffee_price = db.Column(db.String(250), nullable=True)

with app.app_context():
    db.create_all()

@app.route('/')
def home():
    cafes = Cafe.query.all()
    return render_template('index.html', cafes = cafes)

@app.route('/cafe-info/<int:cafe_id>')
def cafe_info(cafe_id):
    cafe = db.session.execute(db.Select(Cafe).where(Cafe.id == cafe_id)).scalar()
    print(type(cafe.coffee_price))
    return render_template('cafe_info.html',cafe=cafe)

if __name__ == '__main__':
    app.run(debug=True,port=8000)